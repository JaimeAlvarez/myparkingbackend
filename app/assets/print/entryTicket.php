<?php
$date = $argv[1];
$barcode = $argv[2];
if($argv[3]){
	$plate = $argv[3];
}

require_once("classes/Escpos.php");
$printer = new Escpos();
$printer -> setJustification(Escpos::JUSTIFY_CENTER);

/* Name of shop */
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer -> text("TECNOPARKING\n");
$printer -> feed();

$printer -> selectPrintMode();
$printer -> text("Fecha y Hora de Ingreso:\n");	
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer -> text($date."\n");
$printer -> feed();

if($plate){
	$printer -> selectPrintMode();
	$printer -> text("Placa:\n");	
	$printer -> selectPrintMode(Escpos::MODE_DOUBLE_WIDTH);
	$printer -> setEmphasis(true);
	$printer -> text($plate."\n");
	$printer -> feed();
}

$printer->setBarcodeHeight ( 120 );
$printer->setBarcodeTextPosition ( Escpos::BARCODE_TEXT_BELOW );
$printer->barcode ( $barcode, Escpos::BARCODE_ITF );

$printer -> cut();
$printer -> close();
?>