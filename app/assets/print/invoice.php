<?php
$consecutive = $argv[1];
$plate_chars = $argv[2];
$entry_date = $argv[3];
$exit_date = $argv[4];
$permanence_time = $argv[5];
$total_amount = $argv[6];
$subtotal = $total_amount * 0.84;
$iva = $total_amount * 0.16;
$cashier = $argv[7];
$barcode = $argv[8];
$invoice_number = $argv[9];

require_once("classes/Escpos.php");
$printer = new Escpos ();
$printer -> setJustification(Escpos::JUSTIFY_CENTER);

/* Name of shop */
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer -> text("TECNOPARKING\n");
$printer -> feed();

/* Name of shop */
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> selectPrintMode();
$printer -> setEmphasis(true);
$printer -> text("FACTURA DE VENTA No. ");
$printer -> text($invoice_number."\n");
$printer -> selectPrintMode();
$printer -> text("Sociedad TRYSEC y CGN\n");
$printer -> text("NIT 890-115.427-5\n");
$printer -> text("Parqueadero Clínica del Norte\n");
$printer -> text("Teléfono 3922876-3002552271\n");
$printer -> text("Res. Dian 200001911808\n");
$printer -> text("Fecha Aut. 2016-03-22\n");
$printer -> text("Numeración Autorizada 685601 2000000\n");
$printer -> feed();

$printer -> selectPrintMode();
$printer -> setEmphasis(true);
$printer -> text("PLACAS: ".$plate_chars."\n");
$printer -> selectPrintMode();
$printer -> text("Ingreso: ".$entry_date."\n");
$printer -> text("Salida:  ".$exit_date."\n");
$printer -> setEmphasis(true);
$printer -> text("Nota: usted cuenta con 20 minutos para salir de las instalaciones.\n");
$printer -> selectPrintMode();
$printer -> text("Permanencia: ".$permanence_time."\n");
$printer -> text("Consecutivo: ".$consecutive."\n");
$printer -> feed();

$printer -> selectPrintMode();
$printer -> text("Cajero: ".$cashier."\n");
$printer -> feed();

$printer -> setJustification(Escpos::JUSTIFY_RIGHT);
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH);
$printer -> text("Subtotal:    $    ".$subtotal."\n");
$printer -> text(" IVA 16%:    $     ".$iva."\n");
$printer -> text("------------------------\n");
$printer -> setEmphasis(true);
$printer -> text("   Total:    $    ".$total_amount."\n");
$printer -> feed();

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer->setBarcodeHeight ( 120 );
$printer->setBarcodeTextPosition ( Escpos::BARCODE_TEXT_BELOW );
$printer->barcode ( $barcode, Escpos::BARCODE_ITF );


$printer -> cut();
$printer -> close();
?>