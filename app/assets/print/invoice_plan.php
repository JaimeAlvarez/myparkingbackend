<?php
$consecutive = $argv[1];
$owner_full_name = $argv[2];
$plan_name = $argv[3];
$expire_date = $argv[4];
$total_amount = $argv[5];
$subtotal = $total_amount * 0.84;
$iva = $total_amount * 0.16;
$cashier = $argv[6];
$barcode = $argv[7];
$invoice_number = $argv[8];

require_once("classes/Escpos.php");
$printer = new Escpos ();
$printer -> setJustification(Escpos::JUSTIFY_CENTER);

/* Name of shop */
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer -> text("TECNOPARKING\n");
$printer -> feed();

/* Name of shop */
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> selectPrintMode();
$printer -> setEmphasis(true);
$printer -> text("FACTURA DE VENTA No. ");
$printer -> text($invoice_number."\n");
$printer -> selectPrintMode();
$printer -> text("Sociedad TRYSEC y CGN\n");
$printer -> text("NIT 890-115.427-5\n");
$printer -> text("Parqueadero Clínica del Norte\n");
$printer -> text("Teléfono 3922876-3002552271\n");
$printer -> text("Res. Dian 200001911808\n");
$printer -> text("Fecha Aut. 2016-03-22\n");
$printer -> text("Numeración Autorizada 685601 2000000\n");
$printer -> feed();

$printer -> selectPrintMode();
$printer -> setEmphasis(true);
$printer -> text("Cliente: ".$owner_full_name."\n");
$printer -> selectPrintMode();
$printer -> text("Plan: ".$plan_name."\n");
$printer -> text("Fecha y hora de vencimiento: ".$expire_date."\n");
$printer -> text("Consecutivo: ".$consecutive."\n");
$printer -> feed();

$printer -> selectPrintMode();
$printer -> text("Cajero: ".$cashier."\n");
$printer -> feed();

$printer -> setJustification(Escpos::JUSTIFY_RIGHT);
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH);
$printer -> text("Subtotal:$ ".$subtotal."\n");
$printer -> text(" IVA 16%:$ ".$iva."\n");
$printer -> text("------------------------\n");
$printer -> setEmphasis(true);
$printer -> text("   Total:$ ".$total_amount."\n");
$printer -> feed();

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer->setBarcodeHeight ( 120 );
$printer->setBarcodeTextPosition ( Escpos::BARCODE_TEXT_BELOW );
$printer->barcode ( $barcode, Escpos::BARCODE_ITF );


$printer -> cut();
$printer -> close();
?>