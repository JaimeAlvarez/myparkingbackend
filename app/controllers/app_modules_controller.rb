class AppModulesController < ApplicationController
	before_filter :obtain_appmodule, only: [:show, :update]
  
  def index
  	per_page = params[:per_page] || 25
  	page = params[:page] || 1
  	
  	appmodules = AppModule.page(page).per(per_page)
  	render json: appmodules, each_serializer: AppModuleSerializer, root: :collection, meta: pagination_metas(appmodules), status: :ok 
  end

  def create
  	appmodule = AppModule.new(appmodule_params)
  	if appmodule.save
  		render json: appmodule, status: :ok
  	else
  		respond_with_error
  	end
  end

   def update
    appmodule = AppModule.new(appmodule_params)
    if appmodule.update
      render json: appmodule, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    appmodule = AppModule.new(appmodule_params)
    if appmodule.destroy
      render json: appmodule, status: :ok
    else
      respond_with_error
    end
  end

  def show
	render json: @appmodule, status: :ok
  end

  private
  def obtain_appmodule
  	@appmodule = AppModule.find(params[:id])
  end

  def appmodule_params
  	params.require(:appmodule).permit(:name, :order_index, :order_index_list, :description)
  end

end
