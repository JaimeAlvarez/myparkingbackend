class AppPermissionsController < ApplicationController
	before_filter :obtain_app_permission, only: [:show, :update, :destroy]

	def index
		per_page = params[:per_page] || 25
		page = params[:page] || 1

		app_permission = AppPermission.page(page).per(per_page)
		render json: app_permission, each_serializer: AppPermissionSerializer, root: :collection, meta: pagination_metas(app_permission), status: :ok

	end

	def create
		app_permission = AppPermission.new(app_permission_params)
		if app_permission.save
			render json: app_permission, status: :ok
		else
			respond_with_error
		end
	end

	
	def update
		if @app_permission.update(app_permission_params)
			render json: @app_permission, status: :ok
		else
			respond_with_error
		end

	end

	def destroy
		if @app_permission.destroy(app_permission_params)
			render json: @app_permission, status: :ok
		else
			respond_with_error
		end
	end

	def show
		render json: @app_permission, status: :ok
	end


	private
	def obtain_app_permission
		@app_permission = AppPermission.find(params[:id])
	end

	def app_permission_params
		params.require(:app_permission).permit(:name)
	end
end