class ApplicationController < ActionController::Base
  #respond_to :json
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

  # Filters
  #before_filter :require_authentication

  # Requires
  def require_authentication
    authenticate_token || render_unauthorized
  end

  def require_activation
    render_unauthorized unless current_user.activated?
  end

  # Methods
  def render_unauthorized(msg = 'not authorized to perform this action', arg = {})
    render json: { error: true, message: msg }.merge(arg), status: :unauthorized
  end

  def respond_with_success(msg = 'request processed successfully', status = :ok)
    render json: { error: false, message: msg }, status: status
  end

  def respond_with_error(msg = 'request could not be processed due to an error', status = :unprocessable_entity)
    render json: { error: true, message: msg }, status: status
  end

  # Helper Methods
  def pagination_metas(collection)
    page_size = collection.total_pages > 0 ? (collection.total_count.to_f / collection.total_pages).ceil : 0
    { total_entries: collection.total_count, total_pages: collection.total_pages, page_size: page_size }
  end

  def is_authenticated?
    authenticate_token || nil
  end

=begin
  def execute_cronjobs
    tasks, log = execute_cronjob_tasks %w(cronjobs/update_rtb)
    render :text => log.join("\n")
  end
=end
  private
  def authenticate_token
    authenticate_with_http_token do |token, options|
      @current_user = User.find_by_access_token(token)
    end
  end

  # Helper methods for development only
  def write_log(msg)
    logger.debug "----------> #{msg}"
  end # TODO: Remove this method
end
