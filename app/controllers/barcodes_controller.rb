class BarcodesController < ApplicationController
  before_filter :obtain_barcode, only: [:show, :update, :destroy]
  # before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    barcodes = Barcode.page(page).per(per_page)
    render json: barcodes, each_serializer: BarcodeSerializer, root: :collection, meta: pagination_metas(barcodes), status: :ok
  end

  def show
    render json: @barcode, status: :ok
  end

  def update
    if @barcode.update(barcode_params)
      render json: @barcode, status: :ok
    else
      respond_with_error
    end
  end

  def search_by_barcode
    barcode = Barcode.where(barcode: params[:barcode].to_s).first   
    if barcode
      render json: barcode, serializer: BarcodeSerializer, status: :ok
    else
      respond_with_error(msg = 'Barcode not found.')
    end

  end

  private
  def obtain_barcode
    @barcode = Barcode.find(params[:id])
  end

  def barcode_params
    params.require(:barcode).permit(:used, :barcode, :barcodeable_id, :barcodeable_type)
  end
end