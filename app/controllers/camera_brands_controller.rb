class CameraBrandsController < ApplicationController
  def index
  	brands = CameraBrand.all
  	render json: brands, each_serializer: CameraBrandSerializer, root: :brands
  end

  def create
  end
end
