class CameraRefsController < ApplicationController
  def index
  	camera_refs = CameraRef.joins(:camera_brand).merge(CameraBrand.order(brand: :asc)).order(reference: :asc)
  	render json: camera_refs, each_serializer: CameraRefSerializer, root: :cameras
  end

  def create
    if camera_ref = CameraRef.find_or_create_by(camera_brand: CameraBrand.find_or_create_by(brand: params[:camera_ref][:brand]), reference: params[:camera_ref][:reference], path: params[:camera_ref][:path])
      render json: camera_ref
    else
      respond_with_error
    end
  end

  def update
  end

  def edit
  end
end
