class EntriesController < ApplicationController
  require 'open3'
  before_filter :obtain_entry, only: [:show]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    entries = Entry.order(created_at: :desc).page(page).per(per_page)
    render json: entries, each_serializer: EntrySerializer, root: :entries, meta: pagination_metas(entries), status: :ok
  end

  def show
    render json: @entry, status: :ok
  end

  def create
    entry = Entry.new
    entry.set_properties(params)
    if entry.save
      Barcode.create(barcode: Barcode.generate_barcode, barcodeable_id: entry.id, barcodeable_type: "Entry")
      file = Base64.decode64(params[:entry][:photo_url_base64].to_s)
      file = file.force_encoding("UTF-8")
      File.write(Rails.root.join('public','imgs', 'full_photo_'+DateTime.now.to_s+'.jpg'), file)
      render json: entry, status: :ok
    else
      respond_with_error
    end
  end

  private
    def entry_params
      params.require(:entry).permit(:photo_url, :segment_url, :plate_id, :camera_id, :type_of)
    end

    def obtain_entry
      @entry = Entry.find(params[:id])
    end
end
