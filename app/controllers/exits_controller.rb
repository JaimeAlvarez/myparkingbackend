class ExitsController < ApplicationController
  
  def index
  	@exits = Exit.all
  	render json: @exits, each_serializer: ExitSerializer
  end

  def create
    exit = Exit.new
    exit.setProperties(params[:barcode], params[:plate])
    if exit.save
      Barcode.create(barcode: Barcode.generate_barcode, barcodeable_id: exit.id, barcodeable_type: "Exit")
      exit.entry.update(paid: true)
      exit.printTicket
      render json: exit, status: :ok
    else 
      respond_with_error
    end 
  end
  
  private
  def exit_params
  	params.require(:exit).permit(:entry_id, :camera_id)
  end
end