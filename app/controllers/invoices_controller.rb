class InvoicesController < ApplicationController
  before_filter :obtain_invoice, only: [:show]
  before_filter :find_cashier, only: [:create]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    invoices = Invoice.page(page).per(per_page)
    render json: invoices, each_serializer: InvoiceSerializer, root: :invoices, meta: pagination_metas(invoices), status: :ok
  end

  def show
    render json: @invoice, status: :ok
  end

  def create
    invoice = Invoice.new

    invoice.set_properties_entry(params[:barcode], params[:plate])   if params[:type_of] == "Entry"
    invoice.set_properties_plan(params[:owner_id], params[:plan_id]) if params[:type_of] == "Plan"

    if invoice.save
      Barcode.create(barcode: Barcode.generate_barcode, barcodeable_id: invoice.id, barcodeable_type: "Invoice")
      invoice.printInvoice(params[:current_user_id])
      invoice.set_owner_validity if invoice.invoiceable_type == "Plan"
      invoice.invoiceable.update(paid: true) if invoice.invoiceable_type == "Entry"
      render json: {"invoice": invoice, "current_user_id": params[:current_user_id]}, status: :ok
    else
      respond_with_error
    end
  end

  private
  def obtain_invoice
    @invoice = Invoice.find(params[:id])
  end

  def invoice_params    
    params.require(:invoice).permit(:invoice_number, :consecutive, :total_charge, :void, :invoiceable_id, :invoiceable_type)
  end

  def find_cashier
    @cashier = User.find(params[:current_user_id])
  end
end
