class OwnerCategoriesController < ApplicationController
  before_filter :obtain_owner_category, only: [:show, :update, :destroy]
  # before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    owner_categories = OwnerCategory.page(page).per(per_page)
    render json: owner_categories, each_serializer: OwnerCategorySerializer, root: :collection, meta: pagination_metas(owner_categories), status: :ok
  end

  def create
    owner_category = OwnerCategory.new(owner_category_params)
    if owner_category.save
      render json: owner_category, status: :ok
    else
      respond_with_error
    end
  end

  def show
    render json: @owner_category, status: :ok
  end

  def update
    if @owner_category.update(owner_category_params)
      render json: @owner_category, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @owner_category.destroy
      respond_with_success
    else
      respond_with_error
    end
  end

  private
  def obtain_owner_category
    @owner_category = OwnerCategory.find(params[:id])
  end

  def owner_category_params
    params.require(:owner_category).permit(:name, :description)
  end
end
