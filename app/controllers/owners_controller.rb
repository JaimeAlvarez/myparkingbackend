class OwnersController < ApplicationController
  before_filter :obtain_owner, only: [:show, :update, :destroy]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    owners = Owner.page(page).per(per_page)
    render json: owners, each_serializer: OwnerSerializer, root: :collection, meta: pagination_metas(owners), status: :ok
  end

  def create
    owner = Owner.new(owner_params)
    if owner.save
      owner.setValidity
      render json: owner, status: :ok
    else
      render json: owner.errors
    end
  end

  def search_by_country_id
    owner = Owner.where(country_id: params[:country_id]).first

    if owner
      render json: owner, serializer: OwnerSerializer, status: :ok
    else
      respond_with_error(msg = 'Owner not found.')
    end

  end

  def show
    render json: @owner, status: :ok
  end

  def update
    if @owner.update(owner_params)
      render json: @owner, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @owner.destroy
      respond_with_success
    else
      respond_with_error
    end
  end

  private
  def obtain_owner
    @owner = Owner.find(params[:id])
  end

  def owner_params
    params.require(:owner).permit(:first_name, :last_name, :email, :country_id, :address, :phone_number, :position, :owner_category_id, :active)
  end
end
