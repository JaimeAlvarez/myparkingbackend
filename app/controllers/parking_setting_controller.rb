class ParkingSettingController < ApplicationController
	#before_filter :require_authentication

	def index
		parking_setting = ParkingSetting.first
		render json: parking_setting, each_serializer: ParkingSettingSerializer
	end

	def create
		parking_setting = ParkingSetting.first
		parking_setting.set_accesses(params[:parking_setting][:entries].to_i, params[:parking_setting][:exits].to_i)
		if parking_setting.update(parking_params)
			render json: parking_setting
		else
			respond_with_error
		end
	end

	def accesses
		accesses = Access.where(deleted: false).order(:id)
		render json: accesses
	end

	def save_access
		access = Access.find(params[:access][:id])
		if access.update(access_params)
			render json: access
		else			
			respond_with_error
		end
	end

	private
	def parking_params
		params.require(:parking_setting).permit(:company_name, :nit, :parking_name, :phone_number, :dian_resolution, :autorized_date, :iva, :minutes_to_exit, :entries, :exits)
	end

	def access_params
		params.require(:access).permit(:description, :username, :password, :ip_address, :port, :camera_ref_id)
	end
end
