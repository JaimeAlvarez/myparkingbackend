class PlansController < ApplicationController
  before_filter :obtain_plan, only: [:show, :update, :destroy]
 # before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    plans = Plan.page(page).per(per_page)
    render json: plans, each_serializer: PlanSerializer, root: :plans, meta: pagination_metas(plans), status: :ok
  end

  def create
    plan = Plan.new(plan_params)
    if plan.save
      render json: plan, status: :ok
    else
      respond_with_error
    end
  end

  def show
    render json: @plan, status: :ok
  end

  def update
    if @plan.update(plan_params)
      render json: @plan, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @plan.update(active: false)
      respond_with_success
    else
      respond_with_error
    end
  end

  private
  def obtain_plan
    @plan = Plan.find(params[:id])
  end

  def plan_params
    params.require(:plan).permit(:name, :plan_type_by_days, :quantity, :price, :active)
  end
end
