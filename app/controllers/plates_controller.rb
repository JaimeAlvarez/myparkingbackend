class PlatesController < ApplicationController
  before_filter :obtain_plate, only: [:show, :update, :destroy]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    plates = Plate.page(page).per(per_page)
    render json: plates, each_serializer: PlateSerializer, root: :collection, meta: pagination_metas(plates), status: :ok
  end

  def create
    plate = Plate.new(plate_params)
    if plate.save
      render json: plate, status: :ok
    else
      respond_with_error
    end
  end

  def check_uniqueness_plate_char
    #Metodo para verificar la unicidad de las placas
    #Para llamados Ajax
    plate_chars = params[:plate][:plate_chars]
    if params[:id].nil?
      plate = Plate.where(plate_chars: plate_chars)[0]
      render json: plate ? "false" : "true"
    else
      plate = Plate.find(params[:id])
      if plate.plate_chars == plate_chars
        render json: "true"
      else
        plate = Plate.where(plate_chars: plate_chars)[0]
        render json: plate ? "false" : "true"
      end
    end
  end

  def show
    render json: @plate, status: :ok
  end

  def update
    if @plate.update(plate_params)
      render json: @plate, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @plate.update(active: false)
      respond_with_success
    else
      respond_with_error
    end
  end

  def search_by_plate
    plate = Plate.where(plate_chars: params[:plate_chars]).first
    if plate
      render json: plate, status: :ok
    else
      respond_with_error(msg = 'Plate not found.')
    end
  end

  private
  def obtain_plate
    @plate = Plate.find(params[:id])
  end

  def plate_params
    params.require(:plate).permit(:owner_id, :plate_chars)
  end
end
