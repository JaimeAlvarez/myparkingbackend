class SessionsController < ApplicationController
  before_filter :require_authentication, only: :destroy

  def create
    #We do not use Sorcery login method since we do not need a session
    if user = User.authenticate(params[:session][:username], params[:session][:password])
      if user.active
        render json: { access_token: user.access_token, current_user_id: user.id }, status: :ok
      else
        render json: { errors: { message: ['user is not active'] } }, status: :unauthorized
      end
    else
      render json: { errors: { message: ['wrong username or password'] } }, status: :unauthorized
    end
  end

  def destroy
    current_user.reset_access_token!
    respond_with_success
  end
end
