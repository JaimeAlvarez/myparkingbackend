class UserCategoriesController < ApplicationController
  before_filter :obtain_user_category, only: [:show, :update, :destroy]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    user_categories = UserCategory.page(page).per(per_page)
    render json: user_categories, each_serializer: UserCategorySerializer, root: :collection, meta: pagination_metas(user_categories), status: :ok
  end

  def create
    user_category = UserCategory.new(user_category_params)
    if user_category.save
      render json: user_category, status: :ok
    else
      respond_with_error
    end
  end

  def show
    render json: @user_category, status: :ok
  end

  def update
    if @user_category.update(user_category_params)
      render json: @user_category, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @user_category.destroy
      respond_with_success
    else
      respond_with_error
    end
  end

  private
  def obtain_user_category
    @user_category = UserCategory.find(params[:id])
  end

  def user_category_params
    params.require(:user_category).permit(:nombre, :descripcion)
  end
end
