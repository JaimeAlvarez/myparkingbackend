class UserModulePermissionsController < ApplicationController
	before_filter :obtain_user_module_permission, only: [:show, :update]

	def index
		per_page = params[:per_page] || 25
		page = params[:page] || 1

		user_module_permission = UserModulePermission.page(page).per(per_page)
		render json: user_module_permission, each_serializer: UserModulePermissionSerializer, root: :collection, meta: pagination_metas(user_module_permission), status: :ok
	end

	def create
		user_module_permission = UserModulePermission.new(user_module_permission_params)
		if user_module_permission.save
			render json: user_module_permission, status: :ok
		else
			respond_with_error
		end
	end
	
	def update
		if @user_module_permission_params.update
			render json: user_module_permission, status: :ok
		else
			respond_with_error
		end
	end
	
	def destroy
		if @user_module_permission_params.destroy
			render json: user_module_permission, status: :ok
		else
			respond_with_error
		end
	end

	def show
		render json: @user_module_permission, status: :ok
	end

	private
	def obtain_user_module_permission
		@user_module_permission = UserModulePermission.find(params[:id])
	end

	def user_module_permission_params
		params.require(:user_module_permission).permit(:user_id, :app_module_id, :app_permission_id)
	end

end