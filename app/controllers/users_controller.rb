class UsersController < ApplicationController
  before_filter :obtain_user, only: [:show, :update, :destroy]
  #before_filter :require_authentication

  def index
    per_page = params[:per_page] || 25
    page = params[:page] || 1

    users = User.page(page).per(per_page)
    render json: users, each_serializer: UserSerializer, root: :collection, meta: pagination_metas(users), status: :ok
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: :ok
    else
      render json: user.errors.full_messages
    end
  end

  def show
    render json: @user, status: :ok
  end

  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      respond_with_error
    end
  end

  def destroy
    if @user.destroy
      respond_with_success
    else
      respond_with_error
    end
  end

  #Uniqueness Validations
  def check_uniqueness_country_id
    #Metodo para verificar la unicidad de la cedula de los usuarios
    #Para llamados Ajax
    country_id = params[:user][:country_id]
    if params[:id].nil?
      user = User.where(country_id: country_id)[0]
      respond_to do |format|
        format.json{render json: user ? "false" : "true"}
      end
    else
      user = User.find(params[:id])
      if user.country_id == country_id
        respond_to do |format|
          format.json{render json: "true"}
        end
      else
        user = User.where(country_id: country_id)[0]
        respond_to do |format|
          format.json{render json: user ? "false" : "true"}
        end
      end
    end
  end

  def check_uniqueness_email
    #Metodo para verificar la unicidad del email de los usuarios
    #Para llamados Ajax
    email = params[:user][:email]
    if params[:id].nil?
      user = User.where(email: email)[0]
      respond_to do |format|
        format.json{render json: user ? "false" : "true"}
      end
    else
      user = User.find(params[:id])
      if user.email == email
        respond_to do |format|
          format.json{render json: "true"}
        end
      else
        user = User.where(email: email)[0]
        respond_to do |format|
          format.json{render json: user ? "false" : "true"}
        end
      end
    end
  end

  private
  def obtain_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :user_category_id, :active, :country_id)
  end
end
