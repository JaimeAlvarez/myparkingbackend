class Barcode < ActiveRecord::Base
	belongs_to :barcodeable, polymorphic: true

	def self.generate_barcode
		barcode = sprintf('%010d', rand(10**10))
		if Barcode.exists?(barcode: barcode)
			generate_barcode
		else
			return barcode
		end
	end
end