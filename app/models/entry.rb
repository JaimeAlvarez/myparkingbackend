require 'net/http'
require 'uri'
require 'json'
class Entry < ActiveRecord::Base
	belongs_to :plate
	has_one :owner, through: :plate
	belongs_to :camera
	has_one :barcode, as: :barcodeable
	has_one :invoice, as: :invoiceable
	has_one :exit


	def printBarcode
		days = [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
		months = [ " ", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

		if self.plate
			plate = self.plate.plate_chars.to_s
		else
			plate = ""
		end

		system('/usr/bin/php /home/ebenezer/Documents/parking-test/parking-back-end/app/assets/print/entryTicket.php "' + self.barcode.created_at.strftime("#{days[self.barcode.created_at.wday]}, %d #{months[self.barcode.created_at.month]} %Y \n%I:%M:%S %p") + '" "' + self.barcode.barcode.to_s + '" "' + plate + '" | /bin/netcat 192.168.1.225 9100')
	end

	def set_properties(params)
		if params[:entry][:plate_chars]
			plate = Plate.find_or_create_by(plate_chars: params[:entry][:plate_chars].upcase)
			self.plate = plate
		end
		#full_photo_url = Rails.root.join('public','imgs', 'full_photo_'+DateTime.now.to_s+'.jpg')
    	self.photo_url = "http://localhost:140/imgs/full_photo_#{DateTime.now.to_s}.jpg" #full_photo_url
		self.segment_url = "http://localhost:140/imgs/full_photo_#{DateTime.now.to_s}.jpg"
		self.camera_id = params[:entry][:camera_id]
	end
end
