class Exit < ActiveRecord::Base
	belongs_to :entry
	has_one :plate, through: :entry
	has_one :camera
	has_one :barcode, as: :barcodeable

	def printTicket
		days = [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
		months = [ " ", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]


		system('/usr/bin/php /home/ebenezer/Documents/parking-test/parking-back-end/app/assets/print/exitTicket.php "' + self.barcode.created_at.strftime("#{days[self.barcode.created_at.wday]}, %d #{months[self.barcode.created_at.month]} %Y \n%I:%M:%S %p") + '" "' + self.barcode.barcode.to_s + '" "' + self.entry.plate.plate_chars.to_s + '" | /bin/netcat 192.168.1.225 9100')
	end

	def setProperties(barcode, plate)
		plate = Plate.where(plate_chars: plate.to_s).first
		barcode = Barcode.where(barcode: barcode.to_s).first
		barcode.barcodeable.update(plate_id: plate.id)
		self.entry_id = barcode.barcodeable.id
	end
end