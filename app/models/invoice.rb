class Invoice < ActiveRecord::Base
	belongs_to :invoiceable, polymorphic: true
	belongs_to :personable, polymorphic: true
	has_one :barcode, as: :barcodeable	

	before_create :set_invoice_consecutive
	before_create :set_invoice_number

	def set_invoice_consecutive
	  last_invoice_consecutive = Invoice.maximum(:consecutive)
	  if !last_invoice_consecutive
	  	self.consecutive = 000000
	  else
	  	self.consecutive = last_invoice_consecutive + 1
	  end
	end

	def set_invoice_number
	  last_invoice_number = Invoice.maximum(:invoice_number)
	  if !last_invoice_number
	  	self.invoice_number = 685601
	  else
	  	self.invoice_number = last_invoice_number + 1
	  end
	end

	def time_parked(entry, plate)
		if plate.owner
			if entry.created_at < plate.owner.owner_validity.start_validity_date && entry.created_at < plate.owner.owner_validity.end_validity_date
				if DateTime.now < plate.owner.owner_validity.end_validity_date
					logger.debug "entró 1 \n\n"
					return [(plate.owner.owner_validity.start_validity_date - entry.created_at)]
				end
				if DateTime.now > plate.owner.owner_validity.end_validity_date
					logger.debug "entró 2 \n\n"
					return [(plate.owner.owner_validity.start_validity_date - entry.created_at), (DateTime.now - plate.owner.owner_validity.end_validity_date.to_datetime)]
				end				
			end
			if entry.created_at > plate.owner.owner_validity.start_validity_date && entry.created_at < plate.owner.owner_validity.end_validity_date
				logger.debug "entró 3 \n\n"
				if DateTime.now > plate.owner.owner_validity.end_validity_date
					return [(DateTime.now.to_time - plate.owner.owner_validity.end_validity_date.to_time)]
				end
			end
			if entry.created_at > plate.owner.owner_validity.end_validity_date && entry.created_at > plate.owner.owner_validity.end_validity_date
				logger.debug "entró 4 \n\n"
				return [(DateTime.now.to_time - entry.created_at.to_time)]
			end
		else
			logger.debug "entró 5 \n\n"
			return [(DateTime.now.to_time - entry.created_at.to_time)]				
		end
	end

	def set_properties_entry(barcode, plate)
		plate = Plate.find_or_create_by(plate_chars: plate.to_s)			
		barcode = Barcode.find_by(barcode: barcode.to_s)
		if barcode.barcodeable_type == "Entry"
			entry = barcode.barcodeable
			entry.update(plate_id: plate.id)
			self.invoiceable_id 	= barcode.barcodeable_id
			self.invoiceable_type 	= barcode.barcodeable_type
			time_parked = time_parked(entry, plate)
			if time_parked[1]
				self.total_charge = (((time_parked[0])/(3600.to_f)).ceil * 2000) + (((time_parked[1])/(3600.to_f)).ceil * 2000)
			else
				self.total_charge = ((time_parked[0])/(3600.to_f)).ceil * 2000
				logger.debug "total_charge = #{self.total_charge}" 
			end
		end
		if barcode.barcodeable_type == "Exit"
			barcode.barcodeable.update(expired: true, used: true)
			entry = Entry.create(type_of: "From Exit", expired_exit_id: barcode.barcodeable.id, plate: plate, camera_id: nil)
			entry.update(created_at: (barcode.barcodeable.created_at + 20.minutes))
			self.invoiceable = entry
			self.total_charge = ((time_parked(entry, entry.plate)[0])/3600).ceil * 2000
		end
	end

	def set_properties_plan(owner_id, plan_id)
		owner = Owner.find(owner_id)
		plan = Plan.find(plan_id)
		self.invoiceable_id = plan.id
		self.invoiceable_type = "Plan"
		self.personable_id = owner.id
		self.personable_type = "Owner"
		self.total_charge = plan.price
	end

	def set_owner_validity
		logger.debug "Entró a set_owner_validity"
		self.personable.update(active: true)
      	self.personable.owner_validity.set_validity_date(self.invoiceable.quantity, self.invoiceable.plan_type_by_days)
      	self.personable.owner_validity.save
	end

	def permanence
		time_diff = time_parked(self.invoiceable, self.invoiceable.plate)
		if time_diff[1]
			return Time.at((time_diff[0] + time_diff[1]).ceil).utc.strftime "%H hora(s) %M minuto(s)."
		else
			return Time.at(time_diff[0].ceil).utc.strftime "%H hora(s) %M minuto(s)."			
		end
	end

	def expire_date
		if self.invoiceable.plan_type_by_days
			return Time.now + self.invoiceable.quantity.to_i.days
		end
	end

	def printInvoice(cashier_id)
		cashier = User.find(cashier_id)
		if self.invoiceable_type == "Entry"
			system('/usr/bin/php /home/ebenezer/Documents/parking-test/parking-back-end/app/assets/print/invoice.php "' + self.consecutive.to_s + '" "' + self.invoiceable.plate.plate_chars.to_s + '" "' + self.invoiceable.created_at.to_s + '" "' + self.created_at.to_s + '" "' + self.permanence.to_s + '" "' + self.total_charge.to_s + '" "' + cashier.first_name.to_s + ' ' + cashier.last_name.to_s + '" "' + self.barcode.barcode.to_s  + '" "' + self.invoice_number.to_s  + '" | /bin/netcat 192.168.1.225 9100')

			exit = Exit.create(entry_id: self.invoiceable.id)

			Barcode.create(barcode: Barcode.generate_barcode, barcodeable_id: exit.id, barcodeable_type: "Exit")

			exit.printTicket
		end
		if self.invoiceable_type == "Plan"
			system('/usr/bin/php /home/ebenezer/Documents/parking-test/parking-back-end/app/assets/print/invoice_plan.php "' + 
				self.consecutive.to_s + '" "' + 
				self.personable.full_name.to_s + '" "' + 
				self.invoiceable.name.to_s + '" "' + 
				self.expire_date.to_s + '" "' + 
				self.total_charge.to_s + '" "' + 
				cashier.first_name.to_s + ' ' + cashier.last_name.to_s + '" "' + 
				self.barcode.barcode.to_s  + '" "' + 
				self.invoice_number.to_s  + '" | /bin/netcat 192.168.1.225 9100')
		end
	end
end
