class Owner < ActiveRecord::Base
  #Asociations
  belongs_to :owner_category
  has_many :plates
  has_many :invoices, as: :personable
  has_one :owner_validity

  #General validations
  validates :first_name, :last_name, :address, :phone_number, presence: true
  validates :owner_category_id, numericality: true, presence: true

  #Callbacks
  before_save :format_attributes
  before_create :set_creation_attributes

  def full_name
    self.first_name + ' ' + self.last_name
  end

  def owner_category_name
    owner_category.nil? ? '' : owner_category.name
  end

  def setValidity
    if self.owner_category_id == 1
      OwnerValidity.create(owner_id: self.id, start_validity_date: DateTime.now, end_validity_date: DateTime.now + 100.years)
    else
      OwnerValidity.create(owner_id: self.id)
    end
  end

  private
  def format_attributes
    self.first_name = first_name.strip.titleize.squeeze(" ")
    self.last_name = last_name.strip.titleize.squeeze(" ")
    if !email.nil?
      email.downcase!
    end    
  end

  def set_creation_attributes
    if self.owner_category_id == 1
      self.active = true
    else
      self.active = false
    end
    nil
  end
end
