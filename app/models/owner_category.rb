class OwnerCategory < ActiveRecord::Base
  # General validations
  validates :name, presence: true
end
