class OwnerValidity < ActiveRecord::Base
	belongs_to :owner

	def set_validity_date(quantity, days)
		if self.end_validity_date
			x = (DateTime.now - self.end_validity_date.to_datetime).to_f
			if x <= 0
				if days
					self.end_validity_date += quantity.to_i.days
				else
					self.end_validity_date += quantity.to_i.hours
				end
			else
				if days
					self.start_validity_date = DateTime.now
					self.end_validity_date = DateTime.now + quantity.to_i.days
				else
					self.start_validity_date = DateTime.now
					self.end_validity_date = DateTime.now + quantity.to_i.hours
				end
			end
		else
			if days
				self.start_validity_date = DateTime.now
				self.end_validity_date = DateTime.now + quantity.to_i.days
			else
				self.start_validity_date = DateTime.now
				self.end_validity_date = DateTime.now + quantity.to_i.hours
			end
		end
	end
end
