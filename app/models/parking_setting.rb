class ParkingSetting < ActiveRecord::Base

	def set_accesses(entries, exits)
		entries_inactive = Access.where(type_of: true, active: false).count
		exits_inactive = Access.where(type_of: false, active: false).count
		
		if self.entries < entries
			to_add = entries - self.entries
			to_add.times do |i|
				if entries_inactive > 0
					Access.where(type_of: true, active: false, deleted: true).first.update(active: true, deleted: false)
					entries_inactive -= 1
				else
					Access.create(type_of: true, active: true, deleted: false)
				end
			end
		end

		if self.entries > entries
			to_subtract = self.entries - entries
			to_subtract.times do |i|
				Access.where(type_of: true, active: true, deleted: false).last.update(active: false, deleted: true)
			end
		end

		if self.exits < exits
			to_add = exits - self.exits
			to_add.times do |i|
				if exits_inactive > 0
					Access.where(type_of: false, active: false, deleted: true).first.update(active: true, deleted: false)
					exits_inactive -= 1
				else
					Access.create(type_of: false, active: true, deleted: false)
				end
			end
		end

		if self.exits > exits
			to_subtract = self.exits - exits
			to_subtract.times do |i|
				Access.where(type_of: false, active: true, deleted: false).last.update(active: false, deleted: true)
			end
		end

	end

end
