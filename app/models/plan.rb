class Plan < ActiveRecord::Base
  #Callbacks
  before_create :before_create_callback
  has_many :invoices, as: :invoiceable

  private
  def before_create_callback
    self.active = true
  end
end
