class Plate < ActiveRecord::Base
  #Asociations
  belongs_to :owner
  has_many :entries

  #default_scope
  default_scope{where(active: true)}

  #Validations
  validates :plate_chars, presence: true

  #Callbacks
  before_save :format_attributes
  before_create :before_create_callback

  private
  def format_attributes
    self.plate_chars = self.plate_chars.strip.squeeze("").gsub(' ','').upcase
  end

  def before_create_callback
    active = true
  end
end
