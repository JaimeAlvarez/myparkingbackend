class User < ActiveRecord::Base

  # Associations
  has_many :user_module_permissions
  belongs_to :user_category
  
  # External resources definitions
  authenticates_with_sorcery!
  paginates_per = 50

  # Virtual Attributes
  attr_accessor :password, :password_confirmation

  # General validations
  validates :first_name, :last_name, presence: true
  validates :email, format: { with: /\A[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\z/}, uniqueness: true, presence: true
  validates :country_id, presence: true, uniqueness: true, numericality: true
  validates :user_category_id, numericality: true, presence: true

  # On update Validations
  validates :password, presence: true, confirmation: true, on: :update, if: :password
  validates :password, length: { minimum: 8 }, on: :update, if: :password

  # Callbacks
  before_save :format_attributes
  before_create :generate_access_token
  before_create :set_creation_attributes
  #after_create :deliver_activation_process_instructions!

  # Methods
  def activate!
    assign_attributes(activation_state: 'active')
    save!
  end

  def activated?
    activation_state == 'active'
  end

  def deliver_reset_password_process_instructions!
    deliver_instructions! 'reset_password_email'
  end

  def deliver_activation_process_instructions!
    deliver_instructions! 'activation_needed_email'
  end

  def reset_access_token!
    generate_access_token
    save!
  end

  def full_name
    first_name + ' ' + last_name
  end

  private
  def format_attributes
    self.first_name = first_name.strip.titleize.squeeze(" ")
    self.last_name = last_name.strip.titleize.squeeze(" ")
    email.downcase!
  end

  def set_creation_attributes
    self.active = true
  end

  def generate_access_token
    begin
      self.access_token = SecureRandom.hex(25)
    end while self.class.exists?(access_token: access_token)
  end

  def deliver_instructions!(method)
    #deliver_reset_password_instructions!
    #UserMailer.send(method, self).deliver
  end
end
