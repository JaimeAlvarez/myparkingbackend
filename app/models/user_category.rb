class UserCategory < ActiveRecord::Base
  # General validations
  validates :name, presence: true
  has_many :users
end
