class UserModulePermission < ActiveRecord::Base
	has_one :user
	has_one :app_module
	has_one :app_permission
end
