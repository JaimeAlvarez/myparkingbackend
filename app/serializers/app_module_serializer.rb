class AppModuleSerializer < ActiveModel::Serializer
	attributes :id, :name, :order_index, :order_index_list, :description
	has_many :user_module_permissions
end