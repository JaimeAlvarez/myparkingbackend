class AppPermissionSerializer < ActiveModel::Serializer
	attributes :id, :name
	has_many :user_module_permissions
end