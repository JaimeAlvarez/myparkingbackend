class BarcodeSerializer < ActiveModel::Serializer
  attributes :barcode, :barcodeable_type, :data
  
  def data
  	if object.barcodeable_type == "Entry" || object.barcodeable_type == "Exit"
  		plate = object.barcodeable.plate.plate_chars if object.barcodeable.plate
  		if plate
  			if object.barcodeable.plate.owner
  				owner = {
            "full_name": object.barcodeable.plate.owner.full_name, 
            "country_id": object.barcodeable.plate.owner.country_id,
            "owner_category_id": object.barcodeable.plate.owner.owner_category_id, 
            "active": object.barcodeable.plate.owner.active,
            "validity": {
              "start_validity_date": object.barcodeable.plate.owner.owner_validity.start_validity_date,
              "end_validity_date": object.barcodeable.plate.owner.owner_validity.end_validity_date
            }
          }
  			end 			
  		end
      paid = object.barcodeable.paid if object.barcodeable.has_attribute?(:paid)
      expired = object.barcodeable.expired if object.barcodeable.has_attribute?(:expired)
      used = object.barcodeable.used if object.barcodeable.has_attribute?(:used)
  		return { 
        "plate": plate, 
        "owner": owner,
        "expired": expired,
        "used": used, 
        "paid": paid, 
        "created_at": object.barcodeable.created_at
      }
  	end
  	if object.barcodeable_type == "Invoice"
  		return { "invoice_number": object.barcodeable.invoice_number, "consecutive": object.barcodeable.consecutive, "total_charge": object.barcodeable.total_charge, "description": {"type_of": object.barcodeable.invoiceable_type, "plate": object.barcodeable.invoiceable.plate.plate_chars ,"entry_date": object.barcodeable.invoiceable.created_at, "exit_date": object.barcodeable.invoiceable.exit.created_at}, "created_at": object.barcodeable.created_at }
  	end
  end
end