class CameraRefSerializer < ActiveModel::Serializer
	attributes :id, :brand, :reference, :path

	def brand
		object.camera_brand.brand
	end
end