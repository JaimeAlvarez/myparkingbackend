class EntrySerializer < ActiveModel::Serializer
  attributes :id, :photo_url, :segment_url, :camera, :barcode, :invoice, :paid, :type_of, :created_at
  has_one :plate

  def camera
  	object.camera.description if object.type_of == "normal"
  end

  def barcode
  	object.barcode.barcode if object.type_of == "normal"
  end

  def created_at
  	object.created_at.to_datetime
  end

  def invoice
    object.invoice.invoice_number if object.invoice
  end

  def created_at
    object.barcode.created_at
  end
end
