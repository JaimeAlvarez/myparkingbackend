class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :invoice_number, :type, :total_charge, :description

  def type
  	object.invoiceable_type
  end
  
  def description
    if object.invoiceable_type == "Entry"
    	if object.invoiceable.plate.owner
    		return {
    			"owner_full_name": object.invoiceable.plate.owner.full_name,
          "plate": object.invoiceable.plate.plate_chars
    		}
    	else
    		return {
    			"plate": object.invoiceable.plate.plate_chars
    		}
    	end      
    end
    if object.invoiceable_type == "Plan"
      plates = []
      object.personable.plates.each do |plate|
        plates.push(plate.plate_chars)
      end
      return {
        "owner_full_name": object.personable.full_name,
        "plates": plates
      }
    end
  end
end
