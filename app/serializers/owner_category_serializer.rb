class OwnerCategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
