class OwnerSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :country_id, :address, :phone_number, :active, :owner_category, :plates, :validity

  def validity
  	return {
  		"start_validity_date": object.owner_validity.start_validity_date,
  		"end_validity_date": object.owner_validity.end_validity_date
  	}
  end
end
