class ParkingSettingSerializer < ActiveModel::Serializer
	attributes :company_name, :nit, :parking_name, :phone_number, :dian_resolution, :autorized_date, :iva, :minutes_to_exit, :entries, :exits
end