class PlanSerializer < ActiveModel::Serializer
  attributes :id, :name, :plan_type_by_days, :quantity, :price, :active
end
