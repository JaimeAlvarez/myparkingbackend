class PlateSerializer < ActiveModel::Serializer
  attributes :id, :plate_chars, :owner
  def owner
    if object.owner
      return {
        "owner_full_name": object.owner.full_name,
        "owner_category_id": object.owner.owner_category.id,
        "owner_category": object.owner.owner_category.name,
        "owner_validity": {
          "start_validity_date": object.owner.owner_validity.start_validity_date,
          "end_validity_date": object.owner.owner_validity.end_validity_date
        },
        "owner_active": object.owner.active
      }
    end
  end
end
