class UserCategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
