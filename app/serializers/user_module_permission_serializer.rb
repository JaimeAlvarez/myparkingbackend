class UserModulePermissionSerializer < ActiveModel::Serializer
	attributes :id, :user_id, :app_module_id, :app_permission_id, :created_at, :updated_at
	has_one :user
	has_one :app_module
	has_one :app_permission
end