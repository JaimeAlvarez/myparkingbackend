class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :country_id, :email, :user_category, :active
  has_many :user_module_permissions

  def user_category  	
  	return {
  		"category_id": object.user_category.id,
  		"category_name": object.user_category.name
  	}
  end
end
