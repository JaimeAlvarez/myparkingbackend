Rails.application.routes.draw do

  #Root Application
  root to: 'site#index'

  #Sessions
  post 'login/' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  resources :users, only: [:index, :create, :show, :update, :destroy]
  resources :user_categories, only: [:index, :create, :show, :update, :destroy]
  resources :owner_categories, only: [:index, :create, :show, :update, :destroy]
  resources :owners, only: [:index, :create, :show, :update, :destroy]
  resources :plates, only: [:index, :create, :show, :update, :destroy]
  resources :plans, only: [:index, :create, :show, :update, :destroy]
  resources :app_modules, only: [:index, :show, :create]
  resources :app_permissions, only: [:index, :show, :create, :destroy, :update]
  resources :user_module_permissions, only: [:index, :create, :update, :destroy, :show]
  resources :entries, only:[:index, :show, :create]
  resources :barcodes, only:[:index, :create, :show]
  resources :exits, only:[:index, :create]
  resources :cameras, only:[:index, :create, :show]
  resources :invoices, only:[:index, :show, :create]
  resources :parking_setting, only:[:index, :create]
  resources :camera_refs, only:[:index, :create]
  resources :camera_brands, only:[:index]

  get 'users/check_uniqueness/country_id' => 'users#check_uniqueness_country_id', as: 'check_uniqueness_country_id'
  get 'users/:id/check_uniqueness/country_id' => 'users#check_uniqueness_country_id', as: 'check_uniqueness_country_id_edit'
  get 'users/check_uniqueness/email' => 'users#check_uniqueness_country_id', as: 'check_uniqueness_email'
  get 'users/:id/check_uniqueness/email' => 'users#check_uniqueness_country_id', as: 'check_uniqueness_email_edit'
  get 'plates/check_uniqueness/plate_char' => 'plates#check_uniqueness_plate_char', as: 'check_uniqueness_plate_char'
  get 'plates/:id/check_uniqueness/plate_char' => 'plates#check_uniqueness_plate_char', as: 'check_uniqueness_plate_char_edit'
  get 'owners/search_by_country_id/:country_id' => 'owners#search_by_country_id', as: 'search_owner_by_country_id'
  get 'barcodes/search_by_barcode/:barcode' => 'barcodes#search_by_barcode', as: 'search_barcode_by_barcode'
  get 'plates/search_by_plate/:plate_chars' => 'plates#search_by_plate', as: 'search_plate_by_plate'
  get 'parking_setting/accesses' => 'parking_setting#accesses', as: 'accesses'
  post 'parking_setting/save_access' => 'parking_setting#save_access', as: 'save_access'
end
