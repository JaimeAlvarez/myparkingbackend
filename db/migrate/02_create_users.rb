class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :country_id, null: false, limit: 8
      t.string :crypted_password
      t.string :salt
      t.string :access_token, null: false
      t.references :user_category, null: false
      t.boolean :active, null: false

      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, :country_id, unique: true
  end
end
