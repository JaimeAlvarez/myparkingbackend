class CreateOwnerCategories < ActiveRecord::Migration
  def change
    create_table :owner_categories do |t|
      t.string :name, null: false
      t.text :description

      t.timestamps null: false
    end
  end
end
