class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :country_id, null: false, unique: true
      t.string :address, null: false
      t.string :phone_number, null: false
      t.string :email
      t.references :owner_category, index: true, foreign_key: true
      t.boolean :active

      t.timestamps null: false
    end
  end
end
