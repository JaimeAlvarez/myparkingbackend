class CreatePlates < ActiveRecord::Migration
  def change
    create_table :plates do |t|
      t.references :owner, index: true, foreign_key: true
      t.string :plate_chars, null: false, unique: true
      t.boolean :active, null: false
      
      t.timestamps null: false
    end
  end
end
