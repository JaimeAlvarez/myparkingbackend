class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name, null: false
      t.boolean :plan_type_by_days, null: false, default: true
      t.integer :quantity, null: false
      t.integer :price, null: false
      t.boolean :active, null: false

      t.timestamps null: false
    end
  end
end
