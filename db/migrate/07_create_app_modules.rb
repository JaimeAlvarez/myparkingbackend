class CreateAppModules < ActiveRecord::Migration
  def change
    create_table :app_modules do |t|
      t.string :name
      t.integer :order_index
      t.integer :order_index_list
      t.text :description

      t.timestamps null: false
    end
  end
end
