class CreateUserModulePermissions < ActiveRecord::Migration
  def change
    create_table :user_module_permissions do |t|
      t.integer :user_id
      t.integer :app_module_id
      t.integer :app_permission_id

      t.timestamps null: false
    end

    add_index 'user_module_permissions', ['user_id'], :name => 'index_user_id'

    add_index 'user_module_permissions', ['app_module_id'], :name => 'index_app_module_id'

    add_index 'user_module_permissions', ['app_permission_id'], :name => 'index_app_permission_id'

  end
end
