class CreateParkingSettings < ActiveRecord::Migration
  def change
    create_table :parking_settings do |t|
      t.string :company_name
      t.string :nit
      t.string :parking_name
      t.string :phone_number
      t.string :dian_resolution
      t.date :autorized_date
      t.integer :iva
      t.integer :minutes_to_exit
      t.integer :entries
      t.integer :exits

      t.timestamps null: false
    end
  end
end
