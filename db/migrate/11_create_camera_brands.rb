class CreateCameraBrands < ActiveRecord::Migration
  def change
    create_table :camera_brands do |t|
      t.string :brand, null: false

      t.timestamps null: false
    end
  end
end
