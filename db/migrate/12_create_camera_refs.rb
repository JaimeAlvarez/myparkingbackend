class CreateCameraRefs < ActiveRecord::Migration
  def change
    create_table :camera_refs do |t|
      t.string :reference
      t.string :path
      t.string :protocol
      t.references :camera_brand, null: false, index: true

      t.timestamps null: false
    end
  end
end
