class CreateAccesses < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.boolean :type_of
      t.string :description
      t.string :username
      t.string :password
      t.string :ip_address
      t.string :port
      t.string :state
      t.string :protocol
      t.references :camera_ref, index: true
      t.boolean :active
      t.boolean :deleted

      t.timestamps null: false
    end
  end
end
