class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
    	t.string   :photo_url
    	t.string   :segment_url
      t.boolean  :paid, default: false
      t.string   :type_of, default: "normal"
      t.integer  :expired_exit_id, default: nil

      t.references :plate, index: true, foreign_key: true, null: true
      t.references :access, index: true, foreign_key: true, null: false
      
      t.timestamps null: false
    end
  end
end