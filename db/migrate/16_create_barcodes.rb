class CreateBarcodes < ActiveRecord::Migration
  def change
    create_table :barcodes do |t|
      t.boolean :used, default: true
      t.string :barcode
      t.references :barcodeable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end