class CreateExits < ActiveRecord::Migration
  def change
    create_table :exits do |t|
    	t.string :photo_url
    	t.string :segment_url
    	t.boolean :expired, default: false
      t.boolean :used, default: false
      t.datetime :exit_datetime_in_odroid
		  
      t.references :entry, index: true, foreign_key: true
      t.references :access, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end