class CreateOwnerValidities < ActiveRecord::Migration
  def change
    create_table :owner_validities do |t|
    	t.references :owner, index: true, foreign_key: true
    	t.datetime :start_validity_date
      	t.datetime :end_validity_date

      	t.timestamps null: false
    end
  end
end
