class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
    	t.integer :invoice_number, null: false, unique: true
    	t.integer :consecutive, null: false, unique: true
    	t.integer :total_charge, null: false
    	t.boolean :void, default: false

    	t.references :invoiceable, polymorphic: true, index: true
      t.references :personable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
