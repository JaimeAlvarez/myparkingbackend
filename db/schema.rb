# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 19) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accesses", force: :cascade do |t|
    t.boolean  "type_of"
    t.string   "description"
    t.string   "username"
    t.string   "password"
    t.string   "ip_address"
    t.string   "port"
    t.string   "state"
    t.string   "protocol"
    t.integer  "camera_ref_id"
    t.boolean  "active"
    t.boolean  "deleted"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "accesses", ["camera_ref_id"], name: "index_accesses_on_camera_ref_id", using: :btree

  create_table "app_modules", force: :cascade do |t|
    t.string   "name"
    t.integer  "order_index"
    t.integer  "order_index_list"
    t.text     "description"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "app_permissions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "barcodes", force: :cascade do |t|
    t.boolean  "used",             default: true
    t.string   "barcode"
    t.integer  "barcodeable_id"
    t.string   "barcodeable_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "barcodes", ["barcodeable_type", "barcodeable_id"], name: "index_barcodes_on_barcodeable_type_and_barcodeable_id", using: :btree

  create_table "camera_brands", force: :cascade do |t|
    t.string   "brand",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "camera_refs", force: :cascade do |t|
    t.string   "reference"
    t.string   "path"
    t.string   "protocol"
    t.integer  "camera_brand_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "camera_refs", ["camera_brand_id"], name: "index_camera_refs_on_camera_brand_id", using: :btree

  create_table "entries", force: :cascade do |t|
    t.string   "photo_url"
    t.string   "segment_url"
    t.boolean  "paid",            default: false
    t.string   "type_of",         default: "normal"
    t.integer  "expired_exit_id"
    t.integer  "plate_id"
    t.integer  "access_id",                          null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "entries", ["access_id"], name: "index_entries_on_access_id", using: :btree
  add_index "entries", ["plate_id"], name: "index_entries_on_plate_id", using: :btree

  create_table "exits", force: :cascade do |t|
    t.string   "photo_url"
    t.string   "segment_url"
    t.boolean  "expired",                 default: false
    t.boolean  "used",                    default: false
    t.datetime "exit_datetime_in_odroid"
    t.integer  "entry_id"
    t.integer  "access_id",                               null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "exits", ["access_id"], name: "index_exits_on_access_id", using: :btree
  add_index "exits", ["entry_id"], name: "index_exits_on_entry_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "invoice_number",                   null: false
    t.integer  "consecutive",                      null: false
    t.integer  "total_charge",                     null: false
    t.boolean  "void",             default: false
    t.integer  "invoiceable_id"
    t.string   "invoiceable_type"
    t.integer  "personable_id"
    t.string   "personable_type"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "invoices", ["invoiceable_type", "invoiceable_id"], name: "index_invoices_on_invoiceable_type_and_invoiceable_id", using: :btree
  add_index "invoices", ["personable_type", "personable_id"], name: "index_invoices_on_personable_type_and_personable_id", using: :btree

  create_table "owner_categories", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "owner_validities", force: :cascade do |t|
    t.integer  "owner_id"
    t.datetime "start_validity_date"
    t.datetime "end_validity_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "owner_validities", ["owner_id"], name: "index_owner_validities_on_owner_id", using: :btree

  create_table "owners", force: :cascade do |t|
    t.string   "first_name",        null: false
    t.string   "last_name",         null: false
    t.string   "country_id",        null: false
    t.string   "address",           null: false
    t.string   "phone_number",      null: false
    t.string   "email"
    t.integer  "owner_category_id"
    t.boolean  "active"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "owners", ["owner_category_id"], name: "index_owners_on_owner_category_id", using: :btree

  create_table "parking_settings", force: :cascade do |t|
    t.string   "company_name"
    t.string   "nit"
    t.string   "parking_name"
    t.string   "phone_number"
    t.string   "dian_resolution"
    t.date     "autorized_date"
    t.integer  "iva"
    t.integer  "minutes_to_exit"
    t.integer  "entries"
    t.integer  "exits"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name",                             null: false
    t.boolean  "plan_type_by_days", default: true, null: false
    t.integer  "quantity",                         null: false
    t.integer  "price",                            null: false
    t.boolean  "active",                           null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "plates", force: :cascade do |t|
    t.integer  "owner_id"
    t.string   "plate_chars", null: false
    t.boolean  "active",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "plates", ["owner_id"], name: "index_plates_on_owner_id", using: :btree

  create_table "rates", force: :cascade do |t|
    t.boolean  "active"
    t.integer  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_categories", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "user_module_permissions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "app_module_id"
    t.integer  "app_permission_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "user_module_permissions", ["app_module_id"], name: "index_app_module_id", using: :btree
  add_index "user_module_permissions", ["app_permission_id"], name: "index_app_permission_id", using: :btree
  add_index "user_module_permissions", ["user_id"], name: "index_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name",                 null: false
    t.string   "last_name",                  null: false
    t.string   "email",                      null: false
    t.string   "country_id",       limit: 8, null: false
    t.string   "crypted_password"
    t.string   "salt"
    t.string   "access_token",               null: false
    t.integer  "user_category_id",           null: false
    t.boolean  "active",                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["country_id"], name: "index_users_on_country_id", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "entries", "accesses"
  add_foreign_key "entries", "plates"
  add_foreign_key "exits", "accesses"
  add_foreign_key "exits", "entries"
  add_foreign_key "owner_validities", "owners"
  add_foreign_key "owners", "owner_categories"
  add_foreign_key "plates", "owners"
end
