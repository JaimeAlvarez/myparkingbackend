# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
ParkingSetting.create(company_name: "Unión Temporal Ebenezer-CGN", nit: "890-115.427-5", parking_name: "Parqueado Clínica General del Norte", phone_number: "3002552271", dian_resolution: "200001911808", autorized_date: "2016-03-22", iva: 16, minutes_to_exit: 20, entries: 0, exits: 0)

CameraBrand.create(brand: "HikVision")
CameraBrand.create(brand: "Dahua")

CameraRef.create(reference: "A200", camera_brand_id: 1)
CameraRef.create(reference: "A300", camera_brand_id: 1)
CameraRef.create(reference: "A400", camera_brand_id: 1)
CameraRef.create(reference: "G700", camera_brand_id: 2)
CameraRef.create(reference: "G800", camera_brand_id: 2)

Plan.create(name: 'Plan 30 días.', plan_type_by_days: true, quantity: 30, price: 74000, active: true)
Plan.create(name: 'Plan 60 días.', plan_type_by_days: true, quantity: 60, price: 130000, active: true)
Plan.create(name: 'Plan 90 días.', plan_type_by_days: true, quantity: 90, price: 180000, active: true)

UserCategory.create(name: 'Administrador', description: 'Perfil para Administradores del Sistema')
UserCategory.create(name: 'Operario', description: 'Perfil para Operarios del Sistema')

User.create(first_name: 'Jaime', last_name: 'Alvarez Acuña', email: 'thecto@ebenezertechs.com', country_id: "72339548", password: '12345678', user_category_id: 1)
User.create(first_name: 'Edgar', last_name: 'Florez Ostos', email: 'theceo@ebenezertechs.com', country_id: '12345678', password: '12345678', user_category_id: 1)

OwnerCategory.create(name: 'Personal Administrativo', description: 'Perfil para administrativos')
OwnerCategory.create(name: 'Personal Médico', description: 'Perfil para el cuerpo médico')

Owner.create(first_name: "Jaime", last_name: "Alvarez Acuña", country_id: "72339548", address: "Calle 29A No. 35B-50", phone_number: "3002552271", email: "jaime.alvarez1984@hotmail.com", owner_category_id: 1)
OwnerValidity.create(owner_id: 1, start_validity_date: DateTime.now, end_validity_date: DateTime.now + 100.years)
Plate.create(owner_id: 1, plate_chars: "UYX123", active: true)

Owner.create(first_name: "Edgar", last_name: "Florez de Cabarcas", country_id: "987654321", address: "Calle 96 No. 47-30", phone_number: "30098711274", email: "edgarfra6@gmail.com", owner_category_id: 2)
OwnerValidity.create(owner_id: 2, start_validity_date: DateTime.now - 30.years, end_validity_date: DateTime.now - 30.years)
Plate.create(owner_id: 2, plate_chars: "MXP561", active: true)

Owner.create(first_name: "Juan", last_name: "Zubiría", country_id: "123456789", address: "Calle 96 No. 47-30", phone_number: "30098711274", email: "edgarfra6@gmail.com", owner_category_id: 2)
OwnerValidity.create(owner_id: 3, start_validity_date: DateTime.now - 30.years, end_validity_date: DateTime.now - 30.years)
Plate.create(owner_id: 3, plate_chars: "UYX234", active: true)









